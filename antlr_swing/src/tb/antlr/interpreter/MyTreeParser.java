package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer modulo(Integer a, Integer n) {
		return a % n;
	}
	
	protected Integer power(Integer base, Integer power) {
		return (int)Math.pow(base, power);
	}
	
	protected void declare(String name) {
		if(!localSymbols.hasSymbol(name)) {
			localSymbols.newSymbol(name);
		}
	}
	
	protected void set(String name, Integer value) {
		if(!localSymbols.hasSymbol(name)) {
			declare(name);
		}
		localSymbols.setSymbol(name, value);
	}
	
	protected Integer get(String name) {
		if(localSymbols.hasSymbol(name)) {
			return localSymbols.getSymbol(name);
		}
		return 0;
	}
	
	protected void enter() {
		localSymbols.enterScope();
	}
	
	protected void leave() {
		localSymbols.leaveScope();
	}
	
}
